from .Node import Node
from .Edge import Edge
from collections import OrderedDict
import graphviz

"""
strict digraph G { 
	{
		"aba" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
		"moshirdivan" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
		"mamanbozorg" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
		"balal" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
		"parvin" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
		"farideh" [color=gray80 fontcolor=deepskyblue2 fillcolor=gray95 fontsize=10 fontname=helvetica shape=egg style=filled]
	}
	{ "moshirdivan" "aba" } -> "mamanbozorg" [color=grey60 arrowsize=0.5]
	{ "balal" "mamanbozorg" } -> "parvin" [color=grey60 arrowsize=0.5]
	{ "balal" "mamanbozorg" } -> "farideh" [color=grey60 arrowsize=0.5]
}
"""


class Graph:
	def __init__(self, strict=True):
		self._nodes = OrderedDict()
		self._outward_edges = {}
		self._inward_edges = {}
		self._edges = []
		self._is_strict = strict

	@property
	def edges(self):
		return self._edges.copy()

	@property
	def _graphviz_header(self):
		if self._is_strict:
			return 'strict digraph G {\n'
		else:
			return 'digraph G{\n'

	def get_graphviz_str(self):
		nodes_str = '\t{\n\t\t' + '\n\t\t'.join([node.get_graphviz_str() for node in self._nodes.values()]) + '\n\t}\n'
		edges_str = '\t' + '\n\t'.join([edge.get_graphviz_str() for edge in self.edges]) + '\n'
		return self._graphviz_header + nodes_str + edges_str + '}'

	def render(self):
		return graphviz.Source(self.get_graphviz_str())

	def get_node(self, node):
		"""
		:type node: Node or str
		:rtype: Node
		"""
		if isinstance(node, str):
			return self._nodes[node]
		else:
			return self._nodes[node.name]

	def get_node_name(self, node):
		"""
		:type node: Node or str
		:rtype: Node
		"""
		if isinstance(node, str):
			return self._nodes[node].name
		else:
			return node.name

	def get_outward_edges(self, node):
		"""
		:type node: Node or str
		:rtype: list[Edge]
		"""
		return self._outward_edges[self.get_node_name(node)].copy()

	def get_inward_edges(self, node):
		"""
		:type node: Node or str
		:rtype: list[Edge]
		"""
		return self._inward_edges[self.get_node_name(node)].copy()

	def get_edges(self, node):
		"""
		:type node: Node or str
		:rtype: list[Edge]
		"""
		return self.get_outward_edges(node=node)+self.get_inward_edges(node=node)

	def get_parents(self, node):
		"""
		:type node: Node or str
		:rtype: list[Node]
		"""
		return [inward_edge.start_node for inward_edge in self.get_inward_edges(node=node)]

	def get_children(self, node):
		"""
		:type node: Node or str
		:rtype: list[Node]
		"""
		return [outward_edge.end_node for outward_edge in self.get_outward_edges(node=node)]

	def _get_ancestors(self, node, nodes_travelled=None):
		"""
		:type node: Node or str
		:type nodes_travelled: list[Node] or None
		:rtype: list[Node]
		"""
		node = self.get_node(node)
		nodes_travelled = nodes_travelled or []
		nodes_travelled.append(node)
		parents = self.get_parents(node=node)
		if len(parents) == 0:
			return [], {}
		else:
			ancestors = []
			ancestors_dict = {1:[]}
			for parent in parents:
				if parent not in nodes_travelled:
					if parent not in ancestors:
						ancestors.append(parent)
						ancestors_dict[1].append(parent)
					parent_ancestors, parent_ancestors_dict = self._get_ancestors(node=parent, nodes_travelled=nodes_travelled)
					for ancestor in parent_ancestors:
						if ancestor not in ancestors:
							ancestors.append(ancestor)
							for distance in parent_ancestors_dict.keys():
								ancestors_dict[distance+1] = parent_ancestors_dict[distance]
			return ancestors, ancestors_dict

	def get_ancestors(self, node, distance=True):
		ancestors, ancestors_dict = self._get_ancestors(node=node, nodes_travelled=None)
		if distance:
			return ancestors_dict
		else:
			return ancestors

	def _get_descendants(self, node, nodes_travelled=None):
		"""
		:type node: Node or str
		:type nodes_travelled: list[Node] or None
		:rtype: list[Node]
		"""
		node = self.get_node(node)
		nodes_travelled = nodes_travelled or []
		nodes_travelled.append(node)
		children = self.get_children(node=node)
		if len(children) == 0:
			return [], {}
		else:
			descendants = []
			descendants_dict = {1:[]}
			for child in children:
				if child not in nodes_travelled:
					if child not in descendants:
						descendants.append(child)
						descendants_dict[1].append(child)
					child_descendants, child_descendants_dict = self._get_descendants(node=child, nodes_travelled=nodes_travelled)
					for descendant in child_descendants:
						if descendant not in descendants:
							descendants.append(descendant)
							for distance in child_descendants_dict.keys():
								descendants_dict[distance+1] = child_descendants_dict[distance]
			return descendants, descendants_dict

	def get_descendants(self, node, distance=True):
		descendants, descendants_dict = self._get_descendants(node=node, nodes_travelled=None)
		if distance:
			return descendants_dict
		else:
			return descendants

	def get_siblings(self, node):
		"""
		:type node: Node or str
		:rtype: list[Node]
		"""
		node = self.get_node(node)
		parents = self.get_parents(node=node)
		siblings = []
		for parent in parents:
			for parents_child in self.get_children(node=parent):
				if parents_child not in siblings and parents_child != node:
					siblings.append(parents_child)
		return siblings

	def get_spouses(self, node):
		"""
		:type node: Node or str
		:rtype: list[Node]
		"""
		node = self.get_node(node)
		children = self.get_children(node=node)
		spouses = []
		for child in children:
			for childs_parent in self.get_parents(node=child):
				if childs_parent not in spouses and childs_parent != node:
					spouses.append(childs_parent)
		return spouses

	def add_node(self, name, **kwargs):
		if name in self._nodes:
			raise KeyError(f'duplicate node id:"{id}"!')
		node = Node(name=name, graph=self, **kwargs)
		self._nodes[name] = node
		self._outward_edges[name] = []
		self._inward_edges[name] = []
		return node

	def connect(self, start, end, **kwargs):
		if isinstance(start, Node):
			start = start.name
		if isinstance(end, Node):
			end = end.name
		edge = Edge(graph=self, start=start, end=end, **kwargs)
		if edge in self._outward_edges[start]:
			raise ValueError('outward edge already exists!')
		if edge in self._inward_edges[end]:
			raise ValueError('inward edge already exists!')
		self._outward_edges[start].append(edge)
		self._inward_edges[end].append(edge)
		if edge not in self._edges:
			self._edges.append(edge)
		return edge

	def disconnect(self, edge):
		"""
		:type edge: Edge
		"""
		self._outward_edges[edge.start].remove(edge)
		self._inward_edges[edge.end].remove(edge)
		self._edges.remove(edge)
		edge._id = (None, None)
		edge._graph = None

	def remove_node(self, node):
		"""
		:type node: Node
		"""
		if not isinstance(node, Node):
			node = self.get_node(node=node)
		for edge in self.get_edges(node=node):
			edge._id = (None, None)
		for edge in self.get_edges(node=node):
			if edge in self._edges:
				self._edges.remove(edge)

		del self._outward_edges[node.name]
		del self._inward_edges[node.name]
		node._graph = None
		del self._nodes[node.name]




