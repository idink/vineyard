from .GraphObj import GraphObj


class Edge(GraphObj):
	def __init__(self, graph, start, end, label=None, style=None, **kwargs):
		super().__init__(graph=graph, id=(start, end), label=label, style=style, **kwargs)

	def __str__(self):
		return f'Edge:{self.start}-{self.end}'

	def __repr__(self):
		return str(self)

	def get_graphviz_str(self):
		without_style = f'"{self.start_node.label}" -> "{self.end_node.label}"'
		if self.style is None:
			return without_style
		else:
			return without_style + ' ' + self.style.get_graphviz_str()

	@property
	def start(self):
		return self.id[0]

	@property
	def end(self):
		return self.id[1]

	@property
	def start_node(self):
		"""
		:rtype: Node
		"""
		return self.graph.get_node(node=self.start)

	@property
	def end_node(self):
		"""
		:rtype: Node
		"""
		return self.graph.get_node(node=self.end)

