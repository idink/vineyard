from .GraphObj import GraphObj


class Node(GraphObj):
	def __init__(self, graph, name, label=None, style=None, **kwargs):
		super().__init__(graph=graph, id=name, label=label, style=style, **kwargs)

	@property
	def name(self):
		return self.id

	@property
	def label(self):
		return self._label or str(self.name)

	def __str__(self):
		return f'Node:{self.name}'

	def __repr__(self):
		return str(self)

	def get_graphviz_str(self):
		if self.style is None:
			return self.label
		else:
			return self.label + ' ' + self.style.get_graphviz_str()

	def connect_to(self, node, **kwargs):
		"""
		:type node: Node
		:rtype: Edge
		"""
		return self.graph.connect(start=self, end=node, **kwargs)

	def connect_from(self, node, **kwargs):
		"""
		:type node:
		:rtype:
		"""
		return self.graph.connect(start=node, end=self, **kwargs)

	@property
	def parents(self):
		"""
		:rtype: list[Node]
		"""
		return self.graph.get_parents(node=self)

	@property
	def children(self):
		"""
		:rtype: list[Node]
		"""
		return self.graph.get_children(node=self)

	@property
	def anscestors(self):
		"""
		:rtype: list[Node]
		"""
		return self.graph._get_ancestors(node=self)

	@property
	def descendants(self):
		"""
		:rtype: list[Node]
		"""
		return self.graph._get_descendants(node=self)

	@property
	def outward_edges(self):
		"""
		:rtype: list[Edge]
		"""
		return self.graph.get_outward_edges(node=self)

	@property
	def inward_edges(self):
		"""
		:rtype: list[Edge]
		"""
		return self.graph.get_inward_edges(node=self)






