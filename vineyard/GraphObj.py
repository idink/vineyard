class GraphObj:
	def __init__(self, graph, id, label=None, style=None, **kwargs):
		self._graph = graph
		self._id = id
		self._label = label
		self._style = style
		self._parameters = dict(kwargs)

	def get(self, item):
		return self._parameters[item]

	def set(self, item, value):
		self._parameters[item] = value

	def __getitem__(self, item):
		return self.get(item=item)

	def __setitem__(self, key, value):
		self.set(item=key, value=value)

	@property
	def graph(self):
		return self._graph

	@property
	def id(self):
		return self._id

	@property
	def label(self):
		return self._label

	@label.setter
	def label(self, label):
		self._label = label

	@property
	def style(self):
		return self._style

	@style.setter
	def style(self, style):
		self._style = style

	def __eq__(self, other):
		"""
		:type other: GraphObj
		:rtype: bool
		"""
		if not isinstance(other, self.__class__): raise TypeError(f'"{other}" is of type: {type(other)}')
		if self.graph != other.graph: raise ValueError('two GraphObj from different graphs cannot be compared')
		return self._id == other._id

	def __ne__(self, other):
		"""
		:type other: GraphObj
		:rtype: bool
		"""
		if not isinstance(other, self.__class__): raise TypeError(f'"{other}" is of type: {type(other)}')
		if self.graph != other.graph: raise ValueError('two GraphObj from different graphs cannot be compared')
		return self._id != other._id


